package com.template

import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.getOrThrow
import net.corda.testing.core.TestIdentity
import net.corda.testing.driver.DriverParameters
import net.corda.testing.driver.driver

fun main(args: Array<String>) {
  val bankA = TestIdentity(CordaX500Name("BankA", "", "GB"))

  driver(DriverParameters(startNodesInProcess = true, waitForAllNodesToFinish = true)) {
    startNode(providedName = bankA.name).getOrThrow()
  }
}