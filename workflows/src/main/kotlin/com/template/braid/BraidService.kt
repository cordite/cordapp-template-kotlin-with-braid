package com.template.braid

import com.template.flows.EchoInitiator
import io.cordite.braid.corda.BraidConfig
import io.cordite.braid.corda.CordaUtilities.toVertxFuture
import io.cordite.braid.corda.services.BraidCordaService.Companion.startAllBraidServicesAndBindConfig
import io.cordite.braid.core.rest.RestConfig
import io.cordite.braid.libs.vertx.core.Future
import io.cordite.braid.libs.vertx.core.json.Json.decodeValue
import net.corda.core.identity.CordaX500Name
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.SingletonSerializeAsToken
import net.corda.core.utilities.contextLogger
import java.lang.ClassLoader.getSystemResource

@CordaService
class BraidService(private val appServiceHub: AppServiceHub) : SingletonSerializeAsToken() {
  companion object {
    private val log = contextLogger()
  }

  init {
    loadConfig()?.let { baseConfig ->
      log.info("binding braid config")
      baseConfig
        // bind the flow to the config - this exposes the flow via Json-RPC
        .withFlow(EchoInitiator::class)
        // we currently need explicit bindings for REST because most Corda Flows utilise generics and these tend to be
        // not designed for de/serialisation with OpenAPI / Json-Schema.
        // A future version of Braid will automatically bind these to REST as well
        .withRestConfig(
          RestConfig()
            .withPaths {
              // bind the POST /echo REST method to a handler function
              // this cans e
              post("/echo", this@BraidService::echo)
            }
        )
    }?.let {
      // bind classes that implement [BraidService], including the default Braid Corda services such as the network map
      log.info("binding Braid services, including default Braid Corda services")
      startAllBraidServicesAndBindConfig(it, appServiceHub)
    }?.bootstrapBraid(appServiceHub)
  }

  // bound functions have to be public
  @Suppress("MemberVisibilityCanBePrivate")
  fun echo(msg: String, counterPartyName: CordaX500Name): Future<String> {
    return appServiceHub.startFlow(EchoInitiator(msg, counterPartyName)).returnValue.toVertxFuture()
  }

  /**
   * Braid is a toolkit and not a framework
   * this gives you the programmer more power to decide how you want to use Braid
   * That means that, for now at least, you'll need to provide your own routine to load your config.
   * This could be something that uses the cordapp config, or in this case, loads a config file from the resources
   * using the party common name
   */
  private fun loadConfig(): BraidConfig? {
    val configResourceName = appServiceHub.myInfo.legalIdentities.first().name.organisation.toLowerCase() + ".config"
    log.info("loading braid config $configResourceName")
    return try {
      decodeValue(getSystemResource(configResourceName).readText(), BraidConfig::class.java)
    } catch(er: Throwable) {
      log.info("didn't find braid config $configResourceName")
      null
    }
  }
}