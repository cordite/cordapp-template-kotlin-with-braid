package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.flows.*
import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.unwrap

// *********
// * Flows *
// *********
@InitiatingFlow
@StartableByRPC
@StartableByService
class EchoInitiator(private val msg: String, private val counterPartyName: CordaX500Name) : FlowLogic<String>() {
    @Suspendable
    override fun call(): String {
        val counterParty = this.serviceHub.identityService.wellKnownPartyFromX500Name(counterPartyName)
          ?: error("could not find counter party $counterPartyName")
        val session = initiateFlow(counterParty)
        session.send(msg)
        return session.receive<String>().unwrap { it }
    }
}

@InitiatedBy(EchoInitiator::class)
class EchoResponder(private val counterpartySession: FlowSession) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() {
        val msg = counterpartySession.receive<String>().unwrap { it }
        counterpartySession.send("echo: $msg")
    }
}
