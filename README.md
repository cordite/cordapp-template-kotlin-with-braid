# Kotlin Cordapp Template with Braid

This is a fork of the [Kotlin Cordapp Template](https://github.com/corda/cordapp-template-kotlin) 
with changes to incorporate Braid within the cordapp. 

## What has been changed

* [workflows/build.gradle](workflows/build.gradle) - added `compile "io.cordite.braid:braid-corda:4.5.2-SNAPSHOT"`.
* Altered [Flows.kt](workflows/src/main/kotlin/com/template/flows/Flows.kt) to implement a more interesting Flow for echoing a string between two parties.
* Created [BraidService](workflows/src/main/kotlin/com/template/braid/BraidService.kt) Corda Service which starts braid, using a custom config loader. Plenty of documentation in the code worth reading.
* Created [banka.config](config/test/banka.config) as an example of how to configure Braid using a resource file. Others ways are possible.
* Created [StandaloneNetwork.kt](workflows/src/integrationTest/kotlin/com/template/StandaloneNetwork.kt) to be able to run a simple network to test our work.

